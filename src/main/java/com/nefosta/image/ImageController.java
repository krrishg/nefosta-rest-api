package com.nefosta.image;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ByteArrayResource;
import java.nio.file.Path;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.io.InputStream;

@RestController
public class ImageController {
	
	@GetMapping(value="/uploads/{image}/**")
	public ResponseEntity<Resource> getImage(@PathVariable String image) throws IOException {
		Path path = Paths.get(new File("uploads/" + image).getAbsolutePath());
		ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
		return ResponseEntity.ok()
			.contentType(MediaType.parseMediaType("image/jpeg"))
			.body(resource);
	}
}
