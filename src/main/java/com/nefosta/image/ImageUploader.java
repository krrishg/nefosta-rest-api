package com.nefosta.image;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

public class ImageUploader {
	private static String fileName, extension;

	public static String uploadImage(MultipartFile image) {
		fileName = String.valueOf(System.currentTimeMillis());
		try {
			byte[] bytes = image.getBytes();
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(getUniqueFile(fileName)));
			out.write(bytes);
			out.flush();
			out.close();
		} catch (IOException e) {e.printStackTrace();}
		return getNewFileLocation();
	}

	private static String getExtension(String fileName) {
		return fileName.split("\\.")[1];
	}

	private static File getUniqueFile(String fileName) {
		return new File("./uploads/" + fileName);
	}

	private static String getNewFileLocation() {
		if (extension == null) extension = "";
		return "/uploads/" + fileName + extension;
	}
}
