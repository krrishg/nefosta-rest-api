package com.nefosta.article;

import com.nefosta.image.ImageUploader;
import com.nefosta.user.ApplicationUser;
import com.nefosta.user.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {
	private ArticleRepository articleRepository;

	@Autowired
	private ApplicationUserRepository applicationUserRepository;

	public ArticleController(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
	}

	@PostMapping()
	public ResponseEntity<Map<String,String>> createArticle(@RequestParam("title") String title,
			@RequestParam("content") String content, 
			@RequestParam("author") String author,
			@RequestParam(value = "photo", required = false) MultipartFile photo) {
		if (content.trim().equals("") || author.trim().equals(""))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Null value(s) not allowed"), HttpStatus.BAD_REQUEST);

		if (isAvailable(author)) {
			Article article = new Article();
			article.setTitle(title);
			content = content.replace("\n", "<br/>");
			article.setContent(content);
			article.setPostedOn(getFormattedCurrentDateTime());
			article.setAuthor(author);

			if (photo != null)
				article.setPhoto(ImageUploader.uploadImage(photo));

			articleRepository.save(article);
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Article created"), HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "User not found: " + author), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/me/**")
	public ResponseEntity<Map<String, Page<Article>>> getMyArticles(@RequestParam(value = "page", required = false, defaultValue="0")int page,
			@RequestParam(value = "limit", required = false, defaultValue = "10")int limit) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ApplicationUser user = applicationUserRepository.findByUsername(principal.toString());

		Pageable paginatedArticles = PageRequest.of(page, limit, Sort.by("id"));
		Page<Article> articles = articleRepository.findByAuthor(user.getUsername(), paginatedArticles);
		return new ResponseEntity<>(
				Collections.singletonMap("msg", articles), HttpStatus.OK);
	}

	@GetMapping("/{id}/**")
	public ResponseEntity<Map<String,Article>> getArticles(@PathVariable long id) {
		Article article = articleRepository.findById(id).get();
		return new ResponseEntity<Map<String,Article>>(
				Collections.singletonMap("msg",article), HttpStatus.OK);
	}

	@GetMapping()
	public Page<Article> getArticles(@RequestParam(value = "page", required = false, defaultValue = "0")int page,
			@RequestParam(value = "limit", required = false, defaultValue = "10")int limit) {
		Pageable paginatedArticles = PageRequest.of(page, limit, Sort.by("id"));
		return articleRepository.findAll(paginatedArticles);
	}

	private boolean isAvailable(String author) {
		ApplicationUser user = applicationUserRepository.findByUsername(author);
		return user!=null;
	}

	private String getFormattedCurrentDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		return dateFormat.format(calendar.getTime());
	}
}
