package com.nefosta.article;

import javax.persistence.*;

@Entity
public class Article {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable=false)
	private String title;

	@Column(nullable=false, columnDefinition = "TEXT")
	private String content;

	@Column(nullable=true)
	private String photo;

	@Column(nullable=false)
	private String postedOn;

	@Column(nullable=false)
	private String author;

	public long getId() {
		return id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPostedOn(String postedOn) {
		this.postedOn = postedOn;
	}

	public String getPostedOn() {
		return postedOn;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}
}
