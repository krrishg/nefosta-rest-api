package com.nefosta.user;

import com.nefosta.image.ImageUploader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private ApplicationUserRepository applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(ApplicationUserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up/**")
    public ResponseEntity<Map<String,String>> signUp(@RequestParam(value = "profilePicture", required=false) MultipartFile profilePicture,
			@RequestParam("email") String email,
			@RequestParam("username") String username,
			@RequestParam("password") String password, 
			@RequestParam("introduction") String introduction, 
			@RequestParam("phoneNumber") String phoneNumber) {

		if (username.trim().equals("") || email.trim().equals("") || 
				password.trim().equals("") || phoneNumber.trim().equals(""))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Null value(s) not allowed"), HttpStatus.BAD_REQUEST);

		if (isShortIntroduction(introduction))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Introduction is too short (Min:25 characters)"), HttpStatus.BAD_REQUEST);

		if (isLongIntroduction(introduction))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Introduction is too long (Max: 250 characters)"), HttpStatus.BAD_REQUEST);

		if (existsUser(username))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "User already exists"), HttpStatus.BAD_REQUEST);

		if (applicationUserRepository.findByEmail(email) != null)
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Email already is already used"), HttpStatus.BAD_REQUEST);

		if (applicationUserRepository.findByPhoneNumber(phoneNumber) != null)
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Phone number is already used"), HttpStatus.BAD_REQUEST);
			
		try {
			ApplicationUser user = new ApplicationUser();
			user.setUsername(username);
			user.setPassword(password);
			user.setEmail(email);
			user.setIntroduction(introduction);
			user.setPhoneNumber(phoneNumber);
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

			if (profilePicture !=null)
				user.setProfilePicture(ImageUploader.uploadImage(profilePicture));
			applicationUserRepository.save(user);

			return new ResponseEntity<>(
					Collections.singletonMap("id", String.valueOf(user.getId())), HttpStatus.CREATED);

		} catch (Exception e) {
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Invalid request" + e.getMessage()), HttpStatus.BAD_REQUEST);
		}
    }

	@PatchMapping("/{id}/**")
	public ResponseEntity<Map<String,String>> updateProfile(
			@PathVariable long id, 
			@RequestBody ApplicationUser user) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ApplicationUser loggedInUser = applicationUserRepository.findByUsername(principal.toString());
		ApplicationUser requestedUser = applicationUserRepository.findById(id).get();
		Assert.notNull(requestedUser, "User not found");

		if (!loggedInUser.getUsername().equals(requestedUser.getUsername()))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Permission denied"), HttpStatus.FORBIDDEN);

		if (existsUser(user.getUsername()))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Username is already used"), HttpStatus.BAD_REQUEST);

		if (isShortIntroduction(user.getIntroduction()))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Introduction is too short (Min:25 characters)"), HttpStatus.BAD_REQUEST);
                                                                                               
		if (isLongIntroduction(user.getIntroduction()))
			return new ResponseEntity<>(
					Collections.singletonMap("msg", "Introduction is too long (Max: 250 characters)"), HttpStatus.BAD_REQUEST);

		requestedUser.setUsername(user.getUsername());
		requestedUser.setIntroduction(user.getIntroduction());
		applicationUserRepository.save(requestedUser);

		return new ResponseEntity<>(
				Collections.singletonMap("msg", "Update successful"), HttpStatus.CREATED);
	}

	private boolean existsUser(String username) {
		return (applicationUserRepository.findByUsername(username) != null);
	}

	private boolean isShortIntroduction(String introduction) {
		return introduction.trim().length()<25;
	}

	private boolean isLongIntroduction(String introduction) {
		return introduction.trim().length()>250;
	}

	@GetMapping("/{id}/**")
	public ResponseEntity<Map<String,UserResponse>> getProfile(@PathVariable long id){
		ApplicationUser user = applicationUserRepository.findById(id).get();
		return getUserProfile(user);
	}

	@GetMapping("/me/**")
	public ResponseEntity<Map<String,UserResponse>> getMyProfile() {
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			ApplicationUser user = applicationUserRepository.findByUsername(principal.toString());
			return getUserProfile(user);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid token", e);
		}
	}

	private ResponseEntity<Map<String,UserResponse>> getUserProfile(ApplicationUser user) {
		UserResponse res = new UserResponse();

		res.setId(user.getId());
		res.setUsername(user.getUsername());
		res.setIntroduction(user.getIntroduction());
		res.setProfilePicture(user.getProfilePicture());

		return new ResponseEntity<>(
				Collections.singletonMap("msg", res), HttpStatus.OK);
	}
}
