package com.nefosta.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
	ApplicationUser findByEmail(String email);
	ApplicationUser findByPhoneNumber(String phoneNumber);
}