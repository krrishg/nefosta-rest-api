package com.nefosta.user;

public class UserResponse {

	private long id;
	private String username, introduction, profilePicture;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getProfilePicture() {
		return profilePicture;
	}
}
