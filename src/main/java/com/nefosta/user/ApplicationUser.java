package com.nefosta.user;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

	@Column(nullable=false, unique=true)
	private String email;

	@Column(nullable=false, unique=true)
    private String username;

	@Column(nullable=false)
    private String password;

	@Column(nullable=false)
	@Size(min=25, max=250)
	private String introduction;

	@Column(nullable=false,unique=true)
	private String phoneNumber;

	@Column(nullable = true)
	private String profilePicture;

    public long getId() {
        return id;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
}
